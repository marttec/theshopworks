Hello there, 

My next scenario is:
```
Black Widow: |----------|
Thor:               |-------------|
Wolverine:                     |-------------|
```

__Given__ Black Widow, Thor, and Wolverine working at FunHouse on Thursday

__When__ Black Widow works in the morning shift

__And__ Thor starts later than Black Widow

__And__ Wolverine starts later than Thor and after Black Widow has finished her shift

__Then__ Black Widow receive single manning supplement until Thor starts his shift

__And__ Thor receives single manning supplement starting when Black Widow has finished her shift, until Wolverine starts his shift

__And__ Wolverine receives single manning supplement starting when Thor has finished his shift, until the end of the day.

My current class handles this fine.

My answers;

1. It took me about 4h 30m. I would change lots of things, e.g. replace arrays
   with collections, but I'd rather tell you all about it than write it down, as this
   could have been longer than the test itself :-)
   
2. I feel more like PHP did choose me than the other way round. Seriously though, 
   it was easy back then to learn it and make money from it, so I just adopted it 
   with all its bright and dark sides and we lived happily ever after.
    
3. Doctrine ORM if I pick Symfony as my most familiar framework. Why? It's simply brilliant
   and very powerful. 
   
4. Frameworks are addictive. Once you start using them you can't stop and very often 
   you end up with 3 pages website or 2-endpoints API built with solutions that 
   are wasted in 99.9%