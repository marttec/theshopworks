# Shopworks Recruitment Test

This test requires PHP 8

To set it up:
```angular2html
$ composer install
```

To run tests:
```angular2html
php vendor/bin/phpunit --bootstrap vendor/autoload.php tests/
```
