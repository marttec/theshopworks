<?php

namespace App\DataTransferObject;

use App\Entity\Rota;

class SingleManning extends AbstractDTO
{
    public array $minutesWorkedAlone;

    /**
     * Get accumulated number of minutes worked alone for a day of a week.
     *
     * @param int $day
     * @return int
     */
    public function getDayMinutes(int $day): int
    {
        return $this->minutesWorkedAlone[$day];
    }
}
