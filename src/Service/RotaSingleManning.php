<?php

namespace App\Service;

use App\DataTransferObject\AbstractDTO;
use App\DataTransferObject\SingleManning;
use App\Entity\Shift;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

class RotaSingleManning
{
    private array $days = [];

    /**
     * @var Shift[]
     */
    private array $shifts;

    /**
     * Process passed Rota to generate Single Manning DTO with minutes per
     * each working Rota day.
     *
     * @param \App\Entity\Rota $rota The Rota to generate the Single Manning for
     * @return AbstractDTO The Single Manning DTO with minutes per Rota days
     */
    public function processRota(\App\Entity\Rota $rota): AbstractDTO
    {
        $this->shifts = $rota->getShifts();

        $this->groupShiftsByRotaWeekDay();

        $singleManningMinutes = $this->processSingleManningMinutesForRotaDays();

        return new SingleManning(
            minutesWorkedAlone: $singleManningMinutes
        );
    }

    /**
     * Calculate the number of single manning minutes for provided Shifts from
     * one Rota's day.
     *
     * @param Shift[] $shifts Array of Shifts from a single Rota's day
     * @return int The number of single manning minutes worked that day
     */
    private function calculateSingleManningMinutes(array $shifts): int
    {
        if (sizeof($shifts) === 1) {
            return $shifts[0]->getTimeWorked();
        }

        $singleManningMinutes = 0;

        $timeEvents = $this->getUniqueTimeEventsFromShifts($shifts);

        foreach ($timeEvents as $key => $event) {
            // The last event of a day will end this loop
            if (false === isset($timeEvents[$key + 1])) {
                break;
            }

            // Form a new period lasting till the next event
            $nextEvent   = $timeEvents[$key + 1];
            $eventPeriod = CarbonPeriod::between($event, $nextEvent);

            $isSingleShiftPeriod = $this->isSingleShiftTimePeriod(
                $eventPeriod,
                $shifts
            );

            if ($isSingleShiftPeriod) {
                $singleManningMinutes += $nextEvent->diffInMinutes($event);
            }
        }

        return $singleManningMinutes;
    }

    /**
     * Check whether the passed period was covered by a single Shift only.
     *
     * @param CarbonPeriod $timePeriod The time period to check
     * @param Shift[] $shifts The Shifts to check
     * @return bool True if only one Shift overlaps this period, false otherwise
     */
    private function isSingleShiftTimePeriod(
        CarbonPeriod $timePeriod,
        array $shifts
    ): bool {
        $shiftsPerPeriodCount = 0;

        foreach ($shifts as $shift) {
            $shiftStartTime = $shift->getStartTime();
            $shiftEndTime   = $shift->getEndTime();

            if ($timePeriod->overlaps($shiftStartTime, $shiftEndTime)) {
                $shiftsPerPeriodCount++;
            }

            // More than 1 Shift per current period will end this loop
            if ($shiftsPerPeriodCount > 1) {
                break;
            }
        }

        if ($shiftsPerPeriodCount === 1) {
            return true;
        }

        return false;
    }

    /**
     * Find unique time events from passed Shifts, i.e. start an end times,
     * and return them ordered by time.
     *
     * @param Shift[] $shifts The Shifts to get time events from
     * @return Carbon[] The sorted array of unique time events
     * @throws \Exception if sorting fails
     */
    private function getUniqueTimeEventsFromShifts(array $shifts): array
    {
        $events = [];

        foreach ($shifts as $shift) {
            $startTime = $shift->getStartTime();
            $endTime   = $shift->getEndTime();

            if (false === in_array($startTime, $events, true)) {
                $events[] = $startTime;
            }

            if (false === in_array($endTime, $events, true)) {
                $events[] = $endTime;
            }
        }

        $sortEvents = function (Carbon $e1, Carbon $e2) {
            return $e1->lessThan($e2) ? -1 : 1;
        };

        $isSorted = usort($events, $sortEvents);

        if (false === $isSorted) {
            throw new \Exception("Sorting Rota day's events failed.");
        }

        return $events;
    }

    /**
     * Process this Rota Shifts by grouping them by a week day.
     */
    private function groupShiftsByRotaWeekDay(): void
    {
        foreach ($this->shifts as $shift) {
            $weekday = $shift->getStartTime()->weekday();

            $this->days[$weekday][] = $shift;
        }
    }

    /**
     * Precess this Rota by evaluating each day's Shifts to calculate
     * the number of single manning minutes.
     *
     * @return array The array with number of single manning minutes per day
     */
    private function processSingleManningMinutesForRotaDays(): array
    {
        $singleManningMinutes = [];

        foreach ($this->days as $day => $shifts) {
            $minutes = $this->calculateSingleManningMinutes($shifts);

            $singleManningMinutes[$day] = $minutes;
        }

        return $singleManningMinutes;
    }
}
