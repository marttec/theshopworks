<?php

namespace App\Entity;

class Rota extends AbstractEntity
{
    public const ROTA_DAY_MONDAY    = 1;
    public const ROTA_DAY_TUESDAY   = 2;
    public const ROTA_DAY_WEDNESDAY = 3;
    public const ROTA_DAY_THURSDAY  = 4;
    public const ROTA_DAY_FRIDAY    = 5;
    public const ROTA_DAY_SATURDAY  = 6;
    public const ROTA_DAY_SUNDAY    = 0;

    /**
     * @var Shift[]
     */
    private array $shifts;

    public function getShifts(): array
    {
        return $this->shifts;
    }
}
