<?php

namespace App\Entity;

use Carbon\Carbon;

class Shift extends AbstractEntity
{
    private Carbon $startTime;
    private Carbon $endTime;

    /**
     * Get the number of minutes a Staff member worked during this Shift.
     * This is calculated from start to end time excluding Shift Breaks.
     *
     * @return int The number of minutes worked by this Shift Staff member
     */
    public function getTimeWorked(): int
    {
        //TODO add implementation
        return 0;
    }

    /**
     * @return Carbon
     */
    public function getStartTime(): Carbon
    {
        return $this->startTime;
    }

    /**
     * @return Carbon
     */
    public function getEndTime(): Carbon
    {
        return $this->endTime;
    }
}
