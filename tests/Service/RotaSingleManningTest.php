<?php

namespace Test\Service;

use App\Entity\Rota;
use App\Entity\Shift;
use App\Service\RotaSingleManning;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;

class RotaSingleManningTest extends TestCase
{
    private RotaSingleManning $service;

    protected function setUp(): void
    {
        $this->service = new RotaSingleManning();
        parent::setUp();
    }

    public function testSingleShiftPerDay(): void
    {
        $shift = $this->createConfiguredMock(Shift::class, [
            'getTimeWorked' => 430,
            'getStartTime'  => Carbon::create(2021, 7, 19, 9),
            'getEndTime'    => Carbon::create(2021, 7, 19, 17),
        ]);

        $rota = $this->createConfiguredMock(Rota::class, [
            'getShifts' => [$shift]
        ]);

        $singleManning = $this->service->processRota($rota);

        $timeWorked = $shift->getTimeWorked();
        $singleManningMinutes = $singleManning->getDayMinutes(
            Rota::ROTA_DAY_MONDAY
        );

        $this->assertEquals(
            $timeWorked,
            $singleManningMinutes,
            'Wrong number of single manning minutes'
        );
    }

    public function testTwoNonOverlappingShiftsPerDay(): void
    {
        $shift1 = $this->createConfiguredMock(Shift::class, [
            'getTimeWorked' => 240,
            'getStartTime'  => Carbon::create(2021, 7, 20, 9),
            'getEndTime'    => Carbon::create(2021, 7, 20, 13),
        ]);

        $shift2 = $this->createConfiguredMock(Shift::class, [
            'getTimeWorked' => 240,
            'getStartTime'  => Carbon::create(2021, 7, 20, 13),
            'getEndTime'    => Carbon::create(2021, 7, 20, 17),
        ]);

        $rota = $this->createConfiguredMock(Rota::class, [
            'getShifts' => [$shift1, $shift2],
        ]);

        $singleManning = $this->service->processRota($rota);

        $combinedTimeWorked = $shift1->getTimeWorked() + $shift2->getTimeWorked();
        $singleManningMinutes = $singleManning->getDayMinutes(
            Rota::ROTA_DAY_TUESDAY
        );

        $this->assertEquals(
            $combinedTimeWorked,
            $singleManningMinutes,
            'Wrong number of single manning minutes'
        );
    }

    public function testTwoOverlappingShiftsPerDay(): void
    {
        $shift1 = $this->createConfiguredMock(Shift::class, [
            'getTimeWorked' => 240,
            'getStartTime'  => Carbon::create(2021, 7, 21, 9),
            'getEndTime'    => Carbon::create(2021, 7, 21, 17),
        ]);

        $shift2 = $this->createConfiguredMock(Shift::class, [
            'getTimeWorked' => 240,
            'getStartTime'  => Carbon::create(2021, 7, 21, 10),
            'getEndTime'    => Carbon::create(2021, 7, 21, 18),
        ]);

        $rota = $this->createConfiguredMock(Rota::class, [
            'getShifts' => [$shift1, $shift2],
        ]);

        $singleManning = $this->service->processRota($rota);

        $singleManningMinutes = $singleManning->getDayMinutes(
            Rota::ROTA_DAY_WEDNESDAY
        );

        $this->assertEquals(
            120, //an hour per each Shift
            $singleManningMinutes,
            'Wrong number of single manning minutes'
        );
    }

    public function testTwoIdenticalShiftsPerDay()
    {
        $shift1 = $this->createConfiguredMock(Shift::class, [
            'getTimeWorked' => 240,
            'getStartTime'  => Carbon::create(2021, 7, 21, 9),
            'getEndTime'    => Carbon::create(2021, 7, 21, 17),
        ]);

        $shift2 = $this->createConfiguredMock(Shift::class, [
            'getTimeWorked' => 240,
            'getStartTime'  => Carbon::create(2021, 7, 21, 9),
            'getEndTime'    => Carbon::create(2021, 7, 21, 17),
        ]);

        $rota = $this->createConfiguredMock(Rota::class, [
            'getShifts' => [$shift1, $shift2],
        ]);

        $singleManning = $this->service->processRota($rota);

        $singleManningMinutes = $singleManning->getDayMinutes(
            Rota::ROTA_DAY_WEDNESDAY
        );

        $this->assertEquals(
            0,
            $singleManningMinutes,
            'Wrong number of single manning minutes'
        );
    }

    public function testThreeOverlappingShiftsWithThreeSingleManningPeriods()
    {
        $shift1 = $this->createConfiguredMock(Shift::class, [
            'getTimeWorked' => 180,
            'getStartTime'  => Carbon::create(2021, 7, 22, 9),
            'getEndTime'    => Carbon::create(2021, 7, 22, 12),
        ]);

        $shift2 = $this->createConfiguredMock(Shift::class, [
            'getTimeWorked' => 240,
            'getStartTime'  => Carbon::create(2021, 7, 22, 11),
            'getEndTime'    => Carbon::create(2021, 7, 22, 15),
        ]);

        $shift3 = $this->createConfiguredMock(Shift::class, [
            'getTimeWorked' => 240,
            'getStartTime'  => Carbon::create(2021, 7, 22, 14),
            'getEndTime'    => Carbon::create(2021, 7, 22, 18),
        ]);

        $rota = $this->createConfiguredMock(Rota::class, [
            'getShifts' => [$shift1, $shift2, $shift3],
        ]);

        $singleManning = $this->service->processRota($rota);

        $singleManningMinutes = $singleManning->getDayMinutes(
            Rota::ROTA_DAY_THURSDAY
        );

        $this->assertEquals(
            120 + 120 + 180,
            $singleManningMinutes,
            'Wrong number of single manning minutes'
        );
    }
}
